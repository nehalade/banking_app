package com.BankApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.BankApplication.entity.Transaction;
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

}
