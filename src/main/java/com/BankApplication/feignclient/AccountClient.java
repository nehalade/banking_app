package com.BankApplication.feignclient;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.BankApplication.dto.AccountRequestDto;
import com.BankApplication.dto.AccountResponseDto;
import com.BankApplication.dto.FundTransferRequestDto;
import com.BankApplication.dto.FundTransferResponseDto;
import com.BankApplication.dto.GpayTransferRequestDto;
import com.BankApplication.dto.GpayTransferResponseDto;



@FeignClient(value = "account-client", url = "http://localhost:8082/account")
public interface AccountClient {
	
	

	
	@PostMapping("/saveAccountDetails")
	public ResponseEntity<AccountResponseDto> saveAccountDetails(@RequestBody AccountRequestDto accountRequestDto);
	
	@PostMapping("/fundTransfer")
	public ResponseEntity<FundTransferResponseDto> sendAmount(@Valid @RequestBody FundTransferRequestDto fundTransferRequestDto);
      
	@PostMapping("/googlePayfundTransfer")
	public ResponseEntity<GpayTransferResponseDto> performGooglePayFundTransfer(@Valid @RequestBody GpayTransferRequestDto googlePayTransferRequestDto);
	
	 @GetMapping("/getAccountNumber")
	public Long getAccountNumberByPhoneNumber(@RequestParam Long phoneNumber);

	

}
