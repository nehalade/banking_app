package com.BankApplication.service;

import com.BankApplication.dto.CustomerRequestDto;
import com.BankApplication.dto.CustomerResponseDto;
import com.BankApplication.dto.FundTransferRequestDto;
import com.BankApplication.dto.FundTransferResponseDto;
import com.BankApplication.dto.GpayTransferRequestDto;
import com.BankApplication.dto.GpayTransferResponseDto;

public interface CustomerService {

	
	public CustomerResponseDto saveCustomer(CustomerRequestDto customerRequestDto);
	public FundTransferResponseDto performFundTransfer(FundTransferRequestDto fundTransferRequestDto);
	public GpayTransferResponseDto performGooglePayFundTransfer(GpayTransferRequestDto googlePayTransferRequestDto);


}
