package com.BankApplication.service;

import java.util.List;

import com.BankApplication.entity.Transaction;


public interface StatementService {
	List<Transaction> getMonthlyEStatement(long accountno, int month, int year);
	List<Transaction> getMonthlyEStatementByPhoneNumber(long phonenumber, int month, int year);

}
