package com.BankApplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.BankApplication.dto.CustomerRequestDto;
import com.BankApplication.dto.CustomerResponseDto;
import com.BankApplication.dto.FundTransferRequestDto;
import com.BankApplication.dto.FundTransferResponseDto;
import com.BankApplication.dto.GpayTransferRequestDto;
import com.BankApplication.dto.GpayTransferResponseDto;
import com.BankApplication.service.CustomerService;


@RestController
@RequestMapping("/transaction")
@Validated
public class CustomerController {
	
	

	@Autowired
	CustomerService customerService;
	
	//Save Customer
	@PostMapping("/saveCustomer")
	public ResponseEntity<CustomerResponseDto> saveCustomer(@RequestBody CustomerRequestDto customerRequestDto) {
		return ResponseEntity.status(HttpStatus.OK).body(customerService.saveCustomer(customerRequestDto));
	}
	
	//Perform Account Fund Transfer
	@PostMapping("/fundTransfer")
	public ResponseEntity<FundTransferResponseDto> performFundTransfer(@Valid @RequestBody FundTransferRequestDto fundTransferRequestDto) {
		return ResponseEntity.status(HttpStatus.OK).body(customerService.performFundTransfer(fundTransferRequestDto));
	
		}
	
	//Perform Account Fund Transfer
	@PostMapping("/googlePayfundTransfer")
	public ResponseEntity<GpayTransferResponseDto> performGooglePayFundTransfer(@Valid @RequestBody GpayTransferRequestDto googlePayTransferRequestDto){
		return ResponseEntity.status(HttpStatus.OK).body(customerService.performGooglePayFundTransfer(googlePayTransferRequestDto));
	}
	

}
