package com.BankApplication.serviceImpl;

import java.time.LocalDate;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.BankApplication.dto.AccountRequestDto;
import com.BankApplication.dto.CustomerRequestDto;
import com.BankApplication.dto.CustomerResponseDto;
import com.BankApplication.dto.FundTransferRequestDto;
import com.BankApplication.dto.FundTransferResponseDto;
import com.BankApplication.dto.GpayTransferRequestDto;
import com.BankApplication.dto.GpayTransferResponseDto;
import com.BankApplication.entity.Customer;
import com.BankApplication.entity.Transaction;
import com.BankApplication.feignclient.AccountClient;
import com.BankApplication.repository.CustomerRepository;
import com.BankApplication.repository.TransactionRepository;
import com.BankApplication.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	AccountClient accountClient;
	
	@Autowired
	TransactionRepository transactionRepository;

	//Save Customer
	@Override
	public CustomerResponseDto saveCustomer(CustomerRequestDto customerRequestDto) {
		// TODO Auto-generated method stub
		Customer customer=new Customer();
		BeanUtils.copyProperties(customerRequestDto, customer);
		Customer mainuser=customerRepository.save(customer);
		CustomerResponseDto customerResponseDto=new CustomerResponseDto();
		BeanUtils.copyProperties(mainuser, customerResponseDto);
		AccountRequestDto accountRequestDto=new AccountRequestDto();
//		accountRequestDto.setAccountType(customerRequestDto.getAccountType());
		accountRequestDto.setBalance(customerRequestDto.getBalance());
		accountRequestDto.setDate(customerRequestDto.getDate());
		accountRequestDto.setPhoneNumber(customerRequestDto.getPhoneNumber());
		accountRequestDto.setAccountId(customerRequestDto.getAccountId());
		accountClient.saveAccountDetails(accountRequestDto).getStatusCode();
		return customerResponseDto;
		//Account createdAccount = accountClient.saveAccountDetails(null)
		
		
	}
	
	//Perform Account Fund Transfer
	public FundTransferResponseDto performFundTransfer(FundTransferRequestDto fundTransferRequestDto) {
		accountClient.sendAmount(fundTransferRequestDto);
		Transaction transaction=new Transaction();
		transaction.setAmount(fundTransferRequestDto.getAmount());
		transaction.setFromAccount(fundTransferRequestDto.getFromAccountNumber());
		transaction.setToAccount(fundTransferRequestDto.getToAccountNumber());
//		transaction.setTransactionType(fundTransferRequestDto.getTransactionType());
		transaction.setTransactionDate(LocalDate.now());
		transactionRepository.save(transaction);
		FundTransferResponseDto fundTransferResponseDto=new FundTransferResponseDto();
		 BeanUtils.copyProperties(transaction,fundTransferResponseDto);
		return fundTransferResponseDto;		
		
	}

	//Perform Account Fund Transfer
	@Override
	public GpayTransferResponseDto performGooglePayFundTransfer(
			GpayTransferRequestDto GpayTransferRequestDto) {
		// TODO Auto-generated method stub
		accountClient.performGooglePayFundTransfer(GpayTransferRequestDto);
		Transaction transaction=new Transaction();
		transaction.setAmount(GpayTransferRequestDto.getAmount());
		transaction.setFromAccount(accountClient.getAccountNumberByPhoneNumber(GpayTransferRequestDto.getFromPhoneNumber()));
		transaction.setToAccount(accountClient.getAccountNumberByPhoneNumber(GpayTransferRequestDto.getToPhoneNumber()));
//		transaction.setTransactionType(googlePayTransferRequestDto.getTransactionType());
		transaction.setTransactionDate(LocalDate.now());
		transactionRepository.save(transaction);
		GpayTransferResponseDto GpayTransferResponseDto=new GpayTransferResponseDto();
		GpayTransferResponseDto.setAmount(transaction.getAmount());
		GpayTransferResponseDto.setFromPhoneNumber(GpayTransferRequestDto.getFromPhoneNumber());
		GpayTransferResponseDto.setToPhoneNumber(GpayTransferRequestDto.getToPhoneNumber());
		GpayTransferResponseDto.setTransactionDate(transaction.getTransactionDate());
//		GpayTransferResponseDto.setTransactionType(transaction.getTransactionType());
		return GpayTransferResponseDto;
	}
	
}
