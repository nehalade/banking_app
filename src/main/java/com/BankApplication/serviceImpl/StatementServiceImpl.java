package com.BankApplication.serviceImpl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.BankApplication.entity.Transaction;
import com.BankApplication.feignclient.AccountClient;
import com.BankApplication.repository.StatementRepository;
import com.BankApplication.service.StatementService;

@Service
public class StatementServiceImpl implements StatementService {

	@Autowired
	StatementRepository statementRepository;
	
	@Autowired
	AccountClient accountClient;

	@Override
	public List<Transaction> getMonthlyEStatement(long accountno, int month, int year) {
		// TODO Auto-generated method stub
		LocalDate startDate=LocalDate.of(year, month, 1);
		LocalDate lastDate=LocalDate.of(year, month, startDate.lengthOfMonth());
		return statementRepository.findByFromAccountOrToAccountAndTransactionDateBetween(accountno, accountno, startDate, lastDate);
		
	}

	@Override
	public List<Transaction> getMonthlyEStatementByPhoneNumber(long phonenumber, int month, int year) {
		// TODO Auto-generated method stub
		Long account=accountClient.getAccountNumberByPhoneNumber(phonenumber);
		LocalDate startDate=LocalDate.of(year, month, 1);
		LocalDate lastDate=LocalDate.of(year, month, startDate.lengthOfMonth());
		//return startDate+" "+lastDate;
		return statementRepository.findByFromAccountOrToAccountAndTransactionDateBetween(account, account, startDate, lastDate);
	
	}

	

}
