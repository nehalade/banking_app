package com.BankApplication.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

public class FundTransferRequestDto {
	

	@NotNull(message = "Balance to be sent cannot Empty")
	private BigDecimal amount;
	@NotNull(message = "Please provide From AccountNumber")
	private Long fromAccountNumber;
	@NotNull(message = "Please provide To AccountNumber")
	private Long toAccountNumber;
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Long getFromAccountNumber() {
		return fromAccountNumber;
	}
	public void setFromAccountNumber(Long fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}
	public Long getToAccountNumber() {
		return toAccountNumber;
	}
	public void setToAccountNumber(Long toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}
}